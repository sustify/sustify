<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
 * Global route parameter constraints: RouteServiceProvider:boot()
 *
 */

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');


/* Test Routes Start */
Route::get('tests/', 'TestsController@showTestPage');
Route::get('tests/media', 'TestsController@testMedia');
/* Test Routes End */

/* Role Start */
Route::get('role/{role_id}/users.{format?}', 'RoleController@roleUsers');
Route::get('role/{role_id}/user/{user_id}.{format?}', 'RoleController@roleUser');
/* Role End */

/* User Start */
Route::get('users', 'User\WebController@index');
Route::get('users/{id}', 'User\WebController@show');
Route::get('users/{id}/roles', 'User\WebController@rolesIndex');
Route::get('users/{user_id}/roles/{role_id}', 'User\WebController@rolesShow');
/* User End */

///* Video Start */
//Route::get('users.{format?}', 'VideoController@index');
//Route::get('users/{id}.{format?}', 'UserController@show');
//
//Route::post('video', 'VideoController@create');
