<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * All routes are prefixed with 'api'
 * see RouteServiceProvider::mapApiRoutes()
 * Example users -> api/users
 */

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

/* User Start */
Route::get('users', 'User\ApiController@index');
Route::get('users/{id}', 'User\ApiController@show');
Route::get('users/{id}/roles', 'User\ApiController@rolesIndex');
Route::get('users/{user_id}/roles/{role_id}', 'User\ApiController@rolesShow');
/* User End */