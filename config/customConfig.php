<?php
/**
 * Created by PhpStorm.
 * User: gerrit
 * Date: 07.12.16
 * Time: 20:54
 */
return [
    'roles' => [
        'user' => 'user',
        'editor' => 'editor',
        'admin' => 'admin',
        'manager' => 'manager'
    ]
];