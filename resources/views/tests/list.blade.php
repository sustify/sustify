@extends('layouts.app')

@section('title', 'List')

@section('content')
    <div class="container">
        <h1>Hallo!</h1>
        <section class="content">
            <ul class="userRoles">
                @foreach($items as $item)
                    <li class="item" id="itemId-{{ $item->id }}">
                        {{ $item->name }}
                    </li>
                @endforeach
            </ul>
        </section>
    </div>
@endsection