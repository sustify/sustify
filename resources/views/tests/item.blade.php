@extends('layouts.app')

@section('title', 'Item')

@section('content')
    <div class="container">
        <h1>Hallo!</h1>
        <section class="content">
            <p class="item" id="itemId-{{ $item->id }}">
                {{ $item->name }}
            </p>
        </section>
    </div>
@endsection