<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sustify - @yield('title')</title>

<link rel="stylesheet" href="{{ URL::asset('/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/css/tea.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/css/awesome-bootstrap-checkbox.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/css/font-awesome-4.6.3/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/js/jquery-ui-1.12.0.custom/jquery-ui.min.css') }}">

{{--<script src="{{ URL::asset('/js/jquery.min.js') }}"></script>--}}

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>
<div id="stage">
    <div id="content">
        @yield('content')
    </div>
</div>
{{--<script src="{{ URL::asset('/js/tea.js') }}"></script>--}}
</body>
</html>
