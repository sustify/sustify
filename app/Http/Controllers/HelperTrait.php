<?php
/**
 * Created by PhpStorm.
 * User: gerrit
 * Date: 09.12.16
 * Time: 15:05
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

trait HelperTrait {

    protected function getListValuesByRequest(Request $request, array $sortCols)
    {
        $offset = $request->get('offset') ?: 0;
        $limit = $request->get('limit') ?: 10;
        $sort = $request->get('sort') ?: $sortCols[0];
        $order = $request->get('order') ?: 'asc';

        $limit = $limit > 1000 ? 10 : $limit;
        $sort = in_array($sort, $sortCols) ? $sort : $sortCols[0];
        $order = in_array($order, ['asc', 'desc']) ? $sort : 'asc';

        return ['offset' => $offset, 'limit' => $limit, 'sort' => $sort, 'order' => $order];
    }

}