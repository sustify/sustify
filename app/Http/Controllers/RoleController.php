<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function roleUsers(int $roleId, string $format = 'json')
    {
        /* @var $roleUsers \Illuminate\Database\Eloquent\Collection */
        $roleUsers = Role::find($roleId)->users;
        if ($roleUsers->count() > 0) {
            if ($format === 'html') {
                return view('tests.tests', ['userRoles' => $roleUsers]);
            } else {
                return $roleUsers->toJson();
            }
        }

        return null;
    }

    public function roleUser(int $roleId, int $userId, string $format = 'json')
    {
        /* @var $user \Illuminate\Database\Eloquent\Collection */
        $user = User::whereHas('roles', function ($q) use ($roleId) {
            $q->where('user_id', $roleId);
        })->where('id', $userId)->get()->first();


        if ($user instanceof User) {
            if ($format === 'html') {
                return view('tests.item', ['item' => $user]);
            } else {
                return json_encode($user->toArray());
            }
        }

        return null;
    }

}
