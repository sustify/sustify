<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\HelperTrait;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    use HelperTrait;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $ret = [];

        /* @var $users \Illuminate\Database\Eloquent\Collection */
        $users = User::getAllPaged($this->getListValuesByRequest($request, User::$sortCols));

        if ($users->count() > 0) {
            $ret = $users->toArray();
        }


        return response()->json($ret);
    }

    public function show(int $userId)
    {
        $ret = [];

        /* @var $user User */
        $user = User::find($userId);
        if ($user instanceof User) {
            $ret = $user->toArray();
        }

        return response()->json($ret);
    }

    public function rolesIndex(int $userId)
    {
        $ret = [];

        /* @var $userRoles \Illuminate\Database\Eloquent\Collection */
        $userRoles = User::findOrFail($userId)->roles;

        if ($userRoles->count() > 0) {
            $ret = $userRoles->toArray();
        }

        return response()->json($ret);
    }

    public function rolesShow(int $userId, int $roleId)
    {
        $ret = [];

        $role = User::getRelatedRoleById($userId, $roleId);

        if ($role instanceof Role) {
            $ret = $role->toArray();
        }

        return response()->json($ret);
    }

}
