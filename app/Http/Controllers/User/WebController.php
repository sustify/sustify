<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HelperTrait;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class WebController extends Controller
{

    use HelperTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, string $format = 'json')
    {
        /* @var $users \Illuminate\Database\Eloquent\Collection */
        $users = User::getAllPaged($this->getListValuesByRequest($request, User::$sortCols));

        return view('tests.list', ['items' => $users]);
    }

    public function show(int $userId)
    {
        /* @var $user User */
        $user = User::findOrFail($userId);

        return view('tests.item', ['item' => $user]);
    }

    public function rolesIndex(int $userId)
    {
        /* @var $userRoles \Illuminate\Database\Eloquent\Collection */
        $userRoles = User::findOrFail($userId)->roles;

        return view('tests.list', ['items' => $userRoles]);
    }

    public function rolesShow(int $userId, int $roleId)
    {
        $role = User::getRelatedRoleById($userId, $roleId);

        return view('tests.item', ['item' => $role]);
    }

}
