<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'nick_name', 'api_token',
    ];

    public static $sortCols = [
        'name', 'email', 'password', 'nick_name', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id', 'roles');
    }

    public static function getAllPaged(array $listValues)
    {
        return self::offset($listValues['offset'])->limit($listValues['limit'])->orderBy($listValues['sort'], $listValues['order'])->get();
    }

    public static function getRelatedRoleById(int $userId, int $roleId)
    {
        /* @var $role \Illuminate\Database\Eloquent\Collection */
        $role = Role::whereHas('users', function (Builder $q) use ($userId) {
            $q->where('user_id', $userId);
        })->where('id', $roleId)->get()->first();

        return $role;
    }
}
